//import org.scalatest._
import collection.mutable.Stack
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.funsuite.AnyFunSuite

// class InitSessionTimes extends AnyFunSuite {

//   import java.time._
//   import slikdag.SchedulesUtils._

//   val zinstant = _zoned_instant("UTC").withHour(12)
//   val initinstant = zinstant.withHour(11).withMinute(0).withSecond(0).plusDays(1)

//   test ("initialize daily session time") {
//     assert(_daily_init_session_time("11:00", zinstant) === initinstant)
//   }
    
// }

class ParseDirectives extends AnyFunSuite {

  import java.io.File

  import io.circe.syntax._
  import io.circe.parser.parse
  import slikdag.DirectiveParsers._
  import slikdag.Directives.{DO, GOTO}

  class EQiFixture {
    val _true = List(1, 1).asJson.hcursor
    val _false = List(1, 2).asJson.hcursor
  }

  def eqi_fixture = new EQiFixture

  test ("parse if _eqi") {
    val json = eqi_fixture
    assert(If.eq[Int](json._true) === true)
    assert(If.eq[Int](json._false) === false)
  }

  test ("parse _nei") {
    val json = eqi_fixture
    assert(If.ne[Int](json._true) === false)
    assert(If.ne[Int](json._false) === true)
  }

  class EQsFixture {
    val _true = List("1", "1").asJson.hcursor
    val _false = List("1", "2").asJson.hcursor
  }

  def eqs_fixture = new EQsFixture

  test ("parse _eqs") {
    val json = eqs_fixture
    assert(If.eq[String](json._true) === true)
    assert(If.eq[String](json._false) === false)
  }

  test ("parse _nes") {
    val json = eqs_fixture
    assert(If.ne[String](json._false) === true)
    assert(If.ne[String](json._true) === false)
  }

  class GTiFixture {
    val _true = List(2, 1).asJson.hcursor
    val _false = List(1, 2).asJson.hcursor
  }

  def gti_fixture = new GTiFixture

  test ("parse _gti") {
    val json = gti_fixture
    assert(If.gt[Int](json._true) === true)
    assert(If.gt[Int](json._false) === false)
  }

  test ("parse _lti") {
    val json = gti_fixture
    assert(If.lt[Int](json._true) === false)
    assert(If.lt[Int](json._false) === true)
  }

  test ("parse _gei") {
    val json = gti_fixture
    assert(If.ge[Int](json._true) === true)
    assert(If.ge[Int](json._false) === false)
  }

  test ("parse _lei") {
    val json = gti_fixture
    assert(If.le[Int](json._true) === false)
    assert(If.le[Int](json._false) === true)
  }

  class GTfFixture {
    val _true = List(2.0, 1.0).asJson.hcursor
    val _false = List(1.0, 2.0).asJson.hcursor
  }

  def gtf_fixture = new GTfFixture

  test ("parse _gts") {
    val json = gtf_fixture
    assert(If.gt[Float](json._true) === true)
    assert(If.gt[Float](json._false) === false)
  }

  test ("parse _lts") {
    val json = gtf_fixture
    assert(If.lt[Float](json._true) === false)
    assert(If.lt[Float](json._false) === true)
  }

  test ("parse _gef") {
    val json = gtf_fixture
    assert(If.ge[Float](json._true) === true)
    assert(If.ge[Float](json._false) === false)
  }

  test ("parse _lef") {
    val json = gtf_fixture
    assert(If.le[Float](json._true) === false)
    assert(If.le[Float](json._false) === true)
  }

  // AND

  class AndFixture {
   val  _true = parse("""{"_eqi_": [1, 1], "_gtf_": [3.0, 1.5]}""").toOption.get.hcursor
   val  _false = parse("""{"_eqi_": [2, 1], "_gtf_": [3.0, 1.5]}""").toOption.get.hcursor
  }

  def and_fixture = new AndFixture

  test ("parse _and") {
    val json = and_fixture
    assert(If.and(json._true) === true)
    assert(If.and(json._false) === false)
  }

  // OR
  class OrFixture {
    val  _true1 = parse("""{"_eqi_": [1, 1], "_gtf_": [0.5, 1.5]}""").toOption.get.hcursor
    val  _true2 = parse("""{"_eqi_": [3, 1], "_gtf_": [2.5, 1.5]}""").toOption.get.hcursor
    val  _false = parse("""{"_eqi_": [2, 1], "_gtf_": [0.5, 1.5]}""").toOption.get.hcursor
  }

  def or_fixture = new OrFixture

  test ("parse _or") {
    val json = or_fixture
    assert(If.or(json._true1) === true)
    assert(If.or(json._true2) === true)
    assert(If.or(json._false) === false)
  }

  // IF

  class IfFixture {
    val _if1 = parse(
      """
{
	"_and_": {
		"_eqi_": [1, 1],
		"_gtf_": [0.5, 1.5]
	},
	"_DO_0": {
                 "_bash_": "echo 1"
                 },
	"_DO_1": {
                 "_bash_": "echo 2"
                 }
}
"""
    ).toOption.get.hcursor
    val _if2 = parse(
      """
{
	"_or_": {
		"_and_": {
                         "_lti_": [1, 1],
		         "_gtf_": [0.5, 1.5]
                },
                "_bool_": true       
	},
	"_DO_0": { 
                 "_bash_": "echo 1"
                 },
	"_DO_1": {
                 "_bash_": "echo 2"
                 }
}
"""
    ).toOption.get.hcursor

  }

  def if_fixture = new IfFixture

  test ("parse _if") {
    val json = if_fixture
    assert(If(json._if1, new File("temp"), List(("a", "a"))) match {
      case DO(p) => p.toString === "[bash, -c, echo 2]"
      case _ => false
    })
    assert(If(json._if2, new File("temp"), List(("a", "a"))) match {
      case DO(p) => p.toString === "[bash, -c, echo 1]"
      case _ => false
    })
  }

  // SWITCH

  class SwitchFixture {
    val _switch1 = parse(
      """
{
	"x": "xyz",
	"zzz": {
		"_DO_": {
                        "_sh_": "echo 1"
                        }
	},
	"xyz": {
		"_DO_": {
                        "_sh_": "echo 2"
                        }
	}
}
"""
    ).toOption.get.hcursor
  }

  def switch_fixture = new SwitchFixture

  test ("_parse_switch") {
    val json = switch_fixture
    assert(Switch(json._switch1, new File("temp"), List(("a", "a"))) match {
      case DO(p) => p.toString === "[sh, -c, echo 2]"
      case _ => false
    })
  }

  // DO

  class DoComposeFixture {

    val _json = parse("""
{
	"_bash_1": "echo 1",
	"_bash_2": "echo 2"
}
""").toOption.get.hcursor

    val _then = " ( [bash, -c, echo 1] ### [bash, -c, echo 2] ) "

    val _and = " ( [bash, -c, echo 1] #&& [bash, -c, echo 2] ) "

    val _or = " ( [bash, -c, echo 1] #|| [bash, -c, echo 2] ) "

    val _pipe = " ( [bash, -c, echo 1] #| [bash, -c, echo 2] ) "

  }

  def compose_fixture = new DoComposeFixture

  test ("parse 'do then'") {
    val json = compose_fixture
    assert(Do.compose("_then_", json._json, new File("temp"), List(("a", "a"))).toString === json._then)
  }

  test("parse 'do and'") {
    val json = compose_fixture
    assert(Do.compose("_and_", json._json, new File("temp"), List(("a", "a"))).toString === json._and)
  }

  test("parse 'do or'") {
    val json = compose_fixture
    assert(Do.compose("_or_", json._json, new File("temp"), List(("a", "a"))).toString === json._or)
  }

  test("parse 'do pipe'") {
    val json = compose_fixture
    assert(Do.compose("_pipe_", json._json, new File("temp"), List(("a", "a"))).toString === json._pipe)
  }

  class DoFixture {

    val _json = parse("""
{
        "_or_": {
                "_bash_0": "echo 1",
                "_then_": {
                         "_bash_3": "echo 2",
                         "_and_": {
                                   "_bash_1": "echo 3",
                                   "_bash_2": "echo 4"
                                   }
                         }
                }                           
}
""").toOption.get.hcursor

    val _do = " ( [bash, -c, echo 1] #||  ( [bash, -c, echo 2] ###  ( [bash, -c, echo 3] #&& [bash, -c, echo 4] )  )  ) "
  }

  def do_fixture = new DoFixture

  test("parse 'do'") {
    val json = do_fixture
    assert(Do(json._json, new File("temp"), List(("a", "a"))).p.toString == json._do)
  }
}
