object Main extends App {

  
  import akka.cluster.ClusterEvent._
  import akka.cluster.MemberStatus
  import akka.cluster.typed._
  import akka.cluster.client.ClusterClientReceptionist
  import akka.management.scaladsl.AkkaManagement

  import slikdag.Messages._
  import slikdag.Agents._
  //import slikdag.Client


  args(0) match {
    case "server" => {
      val cluster = Cluster(slik)
      AkkaManagement(slik).start()
      slik ! Start()
    }
    //case "client" => Client(args.toList.tail)
    case _ => throw new Exception("Unknown")
  }

}

