package slikdag


import akka.management.scaladsl.{ManagementRouteProvider, ManagementRouteProviderSettings}
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

import io.circe.{Decoder, Encoder}
import io.circe.parser.decode
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.syntax._

import scala.util.{Right, Left, Success, Failure}
import scala.concurrent.Promise
import scala.concurrent.ExecutionContext.Implicits.global

import slikdag.RegistersUtils.{tell_to}
import slikdag.Messages._
import slikdag.Tasks.TaskArg

import slikdag.HttpUtils._

class SlikRoutes extends ManagementRouteProvider {

  val KEY = """[A-Za-z0-9_]*.""".r

  def routes (settings: ManagementRouteProviderSettings): Route = {
    pathPrefix("slik") {
      concat(
        path("hello") {
          get {slik_hello()}
        },
        // KV-Store
        pathPrefix("kvstore") {
          concat(
            path("push") {
              post {
                entity(as[String]) {body => KVStore.add(body)}
              }
            },
            path("pop" / KEY) {
              key => get {KVStore.pop(key)}
            },
            path("get" / KEY) {
              key => get {KVStore.get(key)}
            },
            path("list") {
              get {KVStore.list()}
            }
          )
        },
        // Jobs
        pathPrefix("jobs") {
          concat(
            path("register") {
              post {
                entity(as[String]) {body => Jobs.push(body, false)}
              }
            },
            path("initialize" / KEY) {
              key => get {Jobs.initialize(key)}
            },
            path("push") {
              post {
                entity(as[String]) {body => Jobs.push(body, true)}
              }
            },
            path("pop" / KEY) {
              key => get {Jobs.pop(key)}
            },
            path("get-def" / KEY) {
              key => get {Jobs.get_def(key)}
            },
            path("get-status" / KEY) {
              key => get {Jobs.get_status(key)}
            },
            path("get-outcome" / KEY) {
              key => get {Jobs.get_outcome(key)}
            },
            path("list-def") {
              get {Jobs.list_def()}
            },
            path("list-running") {
              get {Jobs.list_running()}
            },
            path("list-outcomes") {
              get {Jobs.list_outcomes()}
            }
          )
        },
        // Schedules
        pathPrefix("schedules") {
          concat(
            path("get" / KEY) {
              key => get {Schedules.get(key)}
            },
            path("stop" / KEY) {
              key => get {Schedules.stop(key)}
            },
            path("pop" / KEY) {
              key => get {Schedules.pop(key)}
            },
            path("list") {
              get {Schedules.list()}
            }
          )
        },
        // Tasks
        pathPrefix("tasks") {
          concat(
            path("get-status" / KEY / KEY) {
              (job, task) => get {Tasks.get_status(job, task)}
            },
            path("get-outcome" / KEY / KEY) {
              (job, task) => get {Tasks.get_outcome(job, task)}
            },
            path("list-outcomes" / KEY) {
              key => get {Tasks.list_outcomes(key)}
            }
              //,
            // path("cancel" / KEY / KEY) {
            //   (job, task) => get {Tasks.cancel(job, task)} //to implement
            // }
          )
        }
      )
    }
  }     
}

object HttpUtils {

  import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
  import io.circe.{Decoder, Encoder}
  import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
  import io.circe.parser.decode
  import io.circe.syntax._

  import akka.http.scaladsl.model.{HttpResponse, HttpEntity}
  import akka.http.scaladsl.server.Route
  import akka.http.scaladsl.model.StatusCodes

  import scala.util.{Try, Either, Right, Left, Success, Failure}
  import scala.concurrent.Promise
  import scala.concurrent.ExecutionContext.Implicits.global

  import slikdag.Tasks.{TaskInstanceStatus, TaskPayload}
  import slikdag.Jobs.{Job, JobDefinition, JobInstanceStatus, JobPayload}
  import slikdag.Schedules.ExecutionTimes

  case class SlikResp(status: Int, body: Option[String] = None)

  implicit val SlikResponseDecoder: Decoder[SlikResp] = deriveDecoder[SlikResp]
  implicit val SlikResponseEncoder: Encoder[SlikResp] = deriveEncoder[SlikResp]

  def slik_response(status: StatusCode, body: Option[String] = None): HttpResponse = {
    val e = SlikResp(status.intValue, body)
    HttpResponse(status=status, entity=HttpEntity(ContentTypes.`application/json`, e.asJson.toString))
  }

  def slik_hello (): Route =
    complete(slik_response(StatusCodes.OK, Some("Hello from SlikDag")))

  object KVStore {

    def add (body: String): Route = {
      val kv = decode[TaskArg](body)
      kv match {
        case Right(arg) => {
          val ans = Promise[Boolean]
          tell_to(AppendTaskArg(arg, ans), "slik")
          onComplete(ans.future) {
            case Success(bool) => {
              if (bool) {
                complete(
                  slik_response(
                    StatusCodes.OK, Some(s"Value appended at key ${arg.key}")
                  )
                )
              } else {
                complete(slik_response(StatusCodes.InternalServerError))
              }
            }
            case Failure(ex) => complete(slik_response(StatusCodes.InternalServerError))
          }
        }
        case Left(ex) => complete(slik_response(
          StatusCodes.BadRequest, Some("TaskArgument is not a valid JSON string")
        ))
      }
    }

    def pop (key: String): Route = {
      val ans = Promise[Option[TaskArg]]
      tell_to(PopTaskArg(key, ans), "slik")
      onComplete(ans.future) {
        case Success(opt) => {
          opt match {
          case Some(arg) =>
              complete(slik_response(StatusCodes.OK, Some(arg.asJson.toString)))
            case None =>
              complete(slik_response(StatusCodes.NotFound, Some(s"${key} not found")))
          }
      }
        case Failure(ex) => complete(slik_response(StatusCodes.InternalServerError, Some("ERROR")))
      }
    }

    def get (key: String): Route = {
      val ans = Promise[Option[TaskArg]]
      tell_to(GetTaskArg(key, ans), "slik")
      onComplete(ans.future) {
        case Success(opt) => {
        opt match {
          case Some(arg) =>
            complete(slik_response(StatusCodes.OK, Some(arg.asJson.toString)))
          case None =>
            complete(slik_response(NotFound, Some(s"${key} not found")))
        }
        }
        case Failure(ex) =>
          complete(slik_response(InternalServerError, Some(ex.getMessage)))
      }
    }

    def list (): Route = {
      val ans = Promise[Either[String,List[String]]]
      tell_to(ListArgs(ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(ls) =>
              complete(slik_response(StatusCodes.OK, Some(ls.asJson.toString)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
        }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }
  }

  object Jobs {

    def _initialize (jbdef: JobDefinition): Route = {
      val ans = Promise[Job]
      tell_to(MakeJob(jbdef, ans), "slik")
      onComplete(ans.future) {
        case Success(jb) => {
          tell_to(ScheduleJob(jb), "slik")
          complete(slik_response(StatusCodes.OK, Some(jb.exec_times.asJson.toString)))
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }

    def initialize (key: String): Route = {
      val ans = Promise[Either[String, JobDefinition]]
      tell_to(GetJobDef(key, ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(jd) =>
              this._initialize(jd)
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }

    def push (body: String, init: Boolean): Route = {
      val ans = Promise[Try[JobDefinition]]
      tell_to(ReadJobDef(body, ans), "slik")
      onComplete(ans.future) {
        case Success(tjd) => {
          tjd match {
            case Success(jd) => {
              tell_to(RegisterJobDef(jd), "slik")
              if (init) this._initialize(jd)
              else complete(slik_response(StatusCodes.OK, Some("correctly registered")))
            }
            case Failure(ex) =>
              complete(slik_response(StatusCodes.BadRequest, Some(ex.getMessage)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }
  
    def get_def (key: String): Route = {
      val ans = Promise[Either[String,JobDefinition]]
      tell_to(GetJobDef(key, ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(jd) =>
              complete(slik_response(StatusCodes.OK, Some(jd.asJson.toString)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }

    def get_status (job: String): Route = {
      val ans = Promise[Either[String,JobInstanceStatus]]
      tell_to(GetJobInstanceStatus(job, ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(js) =>
              complete(slik_response(StatusCodes.OK, Some(js.asJson.toString)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }

    def get_outcome (job: String): Route = {
      val ans = Promise[Either[String,JobPayload]]
      tell_to(GetJobOutcome(job, ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(et) =>
              complete(slik_response(StatusCodes.OK, Some(et.asJson.toString)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }

    def pop (key: String): Route = {
      val ans = Promise[Either[String,JobDefinition]]
      tell_to(PopJobDef(key, ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(jd) =>
              complete(slik_response(StatusCodes.OK, Some(jd.asJson.toString)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }

    def list_def (): Route = {
      val ans = Promise[Either[String,List[String]]]
      tell_to(ListJobDefs(ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(ls) =>
              complete(slik_response(StatusCodes.OK, Some(ls.asJson.toString)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }

    def list_running (): Route = {
      val ans = Promise[Either[String,List[String]]]
      tell_to(ListRunning(ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(ls) =>
              complete(slik_response(StatusCodes.OK, Some(ls.asJson.toString)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }

    def list_outcomes (): Route = {
      val ans = Promise[Either[String,List[String]]]
      tell_to(ListOutcomes(ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(ls) =>
              complete(slik_response(StatusCodes.OK, Some(ls.asJson.toString)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }
  }

  object Schedules {

    def get (key: String): Route = {
      val ans = Promise[Either[String,ExecutionTimes]]
      tell_to(GetExecTimes(key, ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(et) =>
              complete(slik_response(StatusCodes.OK, Some(et.asJson.toString)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }

    def pop (key: String): Route = {
      val ans = Promise[Either[String,ExecutionTimes]]
      tell_to(PopExecTimes(key, ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(et) =>
              complete(slik_response(StatusCodes.OK, Some(et.asJson.toString)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }

    def stop (key: String): Route = {
      val ans = Promise[Either[String, String]]
      tell_to(StopSchedule(key, ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(msg) =>
              complete(slik_response(StatusCodes.OK, Some(msg)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }

    def list (): Route = {
      val ans = Promise[Either[String,List[String]]]
      tell_to(ListExecTimes(ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(ls) =>
            complete(slik_response(StatusCodes.OK, Some(ls.asJson.toString)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }
  }

  object Tasks {

    def list_outcomes (job: String): Route = {
      val ans = Promise[Either[String,List[String]]]
      tell_to(ListJobOutcome(job, ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(ls) =>
              complete(slik_response(StatusCodes.OK, Some(ls.asJson.toString)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }

    def get_outcome (job: String, task: String): Route = {
      val ans = Promise[Either[String,TaskPayload]]
      tell_to(GetTaskOutcome(job, task, ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(et) =>
              complete(slik_response(StatusCodes.OK, Some(et.asJson.toString)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
      case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }

    def get_status (job: String, task: String): Route = {
      val ans = Promise[Either[String,TaskInstanceStatus]]
      tell_to(GetTaskInstanceStatus(job, task, ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(ts) =>
              complete(slik_response(StatusCodes.OK, Some(ts.asJson.toString)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }

    def cancel (hash: String, name: String): Route = {
      val ans = Promise[Either[String, String]]
      tell_to(CancelTask(hash, name, ans), "slik")
      onComplete(ans.future) {
        case Success(e) => {
          e match {
            case Right(msg) =>
              complete(slik_response(StatusCodes.OK, Some(msg)))
            case Left(msg) =>
              complete(slik_response(StatusCodes.NotFound, Some(msg)))
          }
        }
        case Failure(ex) =>
          complete(slik_response(StatusCodes.InternalServerError, Some(ex.getMessage)))
      }
    }
  }
}
