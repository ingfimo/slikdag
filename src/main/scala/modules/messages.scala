package slikdag

object Messages {

  import scala.util.{Try, Either}
  import scala.concurrent.Promise

  import akka.actor.typed.ActorRef

  import slikdag.Tasks._
  import slikdag.Jobs.{JobDefinition, Job, JobInstanceStatus, JobPayload}
  import slikdag.Schedules.ExecutionTimes

  sealed trait Message

  case class Start() extends Message

  case class Stop() extends Message

  case class Info(msg: String) extends Message

  case class Err(msg: String) extends Message

  case class ReadJobDef(jobdef: String, ans: Promise[Try[JobDefinition]]) extends Message

  case class MakeJob(jbdef: JobDefinition, ans: Promise[Job]) extends Message

  case class RegisterJobDef(jbdef: JobDefinition) extends Message

  case class ScheduleJob(jb: Job) extends Message

  case class GetJobDef(key: String, ans: Promise[Either[String,JobDefinition]]) extends Message

  case class PopJobDef(key: String, ans: Promise[Either[String,JobDefinition]]) extends Message

  case class ListJobDefs(ans: Promise[Either[String,List[String]]]) extends Message

  case class RegisterExecTimes(key: String, et: ExecutionTimes) extends Message

  case class GetExecTimes(key: String, ans: Promise[Either[String,ExecutionTimes]]) extends Message

  case class StopSchedule(key: String, ans: Promise[Either[String, String]]) extends Message

  case class PopExecTimes(key: String, ans: Promise[Either[String,ExecutionTimes]]) extends Message

  case class ListExecTimes(ans: Promise[Either[String,List[String]]]) extends Message

  case class ExecTask(hash: String, task: MainTask, env: Map[String, String]) extends Message

  case class ExecJob(jb: Job) extends Message

  case class TaskStatus(hash: String, name: String, status: String) extends Message

  case class TaskOutcome(hash: String, name: String, outcome: Try[String]) extends Message

  case class GetTaskOutcome(hash: String, name: String, ans: Promise[Either[String,TaskPayload]]) extends Message

  case class GetJobOutcome(hash: String, ans: Promise[Either[String,JobPayload]]) extends Message

  case class ListOutcomes(ans: Promise[Either[String,List[String]]]) extends Message

  case class ListJobOutcome(job: String, ans: Promise[Either[String,List[String]]]) extends Message

  case class GetTaskInstanceStatus(job: String, task: String, ans: Promise[Either[String,TaskInstanceStatus]]) extends Message

  case class GetJobInstanceStatus(job: String, ans: Promise[Either[String,JobInstanceStatus]]) extends Message

  case class ListRunning(ans: Promise[Either[String,List[String]]]) extends Message

  case class ListRunningJob(job: String, ans: Promise[Either[String,List[String]]]) extends Message

  case class AppendTaskArg(arg: TaskArg, ans: Promise[Boolean]) extends Message

  case class PopTaskArg(key: String, ans: Promise[Option[TaskArg]]) extends Message

  case class GetTaskArg(key: String, ans: Promise[Option[TaskArg]]) extends Message

  case class ListArgs(ans: Promise[Either[String,List[String]]]) extends Message

  case class FinalizeJob(hash: String) extends Message

  case class CancelTask(hash: String, name: String, ans: Promise[Either[String,String]]) extends Message

}

