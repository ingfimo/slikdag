package slikdag

object Jobs {

  import io.circe.{Decoder, Encoder}
  import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

  import slikdag.Tasks.{Workflow, TaskInstanceStatus, TaskPayload}
  import slikdag.Schedules.{Schedule, ExecutionTimes}

  case class JobDefinition (name: String, schedule: Schedule, workflow: Workflow)

  implicit val JobDecoder: Decoder[JobDefinition] = deriveDecoder[JobDefinition]
  implicit val JobEncoder: Encoder[JobDefinition] = deriveEncoder[JobDefinition]

  case class Job (exec_times: ExecutionTimes, job_def: JobDefinition)

  case class JobInstanceStatus (name: String, status: List[TaskInstanceStatus])

  implicit val JobInstanceStatusEncoder: Encoder[JobInstanceStatus] = deriveEncoder[JobInstanceStatus]

  case class JobPayload (name: String, payload: List[TaskPayload])

  implicit val JobPayloadEncoder: Encoder[JobPayload] = deriveEncoder[JobPayload]
}

object JobsUtils {

  import scala.util.{Right, Left, Success, Failure, Try}
  import scala.reflect.io.Directory

  import java.io.File

  import io.circe.yaml.parser
  import io.circe.parser.decode

  import slikdag.SchedulesUtils.{execution_times}
  import Jobs._

  def read_job_definition (jbdef: String): Try[JobDefinition] = {
    val json = parser.parse(jbdef)
    json match {
      case Right(j) => {
        val job = decode[JobDefinition](j.toString)
        job match {
          case Right(jj) => Success(jj)
          case Left(ex) => Failure(new Exception(ex.getMessage))
        }
      }
      case Left(ex) => Failure(new Exception(ex.getMessage))
    }
  }

  def make_job (jbdef: JobDefinition): Job =
    Job(execution_times(jbdef.schedule), jbdef)
    
  def make_next_job (job: Job): Job =
    Job(execution_times(job.job_def.schedule), job.job_def)

  def make_job_hash (job: Job): String = {
    val hash = s"${job.exec_times.times.head.hashCode()}".replace("-", "0")
    s"${job.job_def.name}_${hash}"
  }

  def make_job_directory (hash: String): Try[String] = {
    Try {
      val wf = new File(hash)
      if (wf.exists) {
        throw new Exception(
          s"cannot make directory for job ${hash}, directory already exists."
        )
      }
      Directory(wf).createDirectory(true)
      hash
    }
  }

  def clean_job_directory (hash: String): Try[String] = {
    Try {
      val wf = new File(hash)
      if (!wf.exists) {
        throw new Exception(
          s"cannot clean directory for job ${hash}, directory does not  exist."
        )
      }
      if (Directory(wf).deleteRecursively) {
        hash
      } else {
        throw new Exception(
          s"unknown error in clening job directory ${hash}"
        )
      }
    }
  }

}
