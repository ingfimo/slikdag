package slikdag

object Actions {

  import scala.concurrent._
  import scala.concurrent.duration._
  import scala.util.{Try, Success, Failure}

  import slikdag.Messages._
  import slikdag.Tasks._
  import slikdag.RegistersUtils.tell_to
  import slikdag.{RegistersUtils => RU}
  import slikdag.TasksUtils._
  import slikdag.Jobs.{JobDefinition, Job, JobInstanceStatus, JobPayload}
  import slikdag.JobsUtils._
  import slikdag.Schedules.ExecutionTimes
  import slikdag.SchedulesUtils.zoned_instant

  object Jobs {

    def read_def (jbdef: String)(implicit ec: ExecutionContext): Future[Try[JobDefinition]] =
      Future(read_job_definition(jbdef))

    def register_def (jbdef: JobDefinition)(implicit ec: ExecutionContext): Future[Unit] =
      Future(RU.Jobs.register_def(jbdef))

    def get_def (key: String)(implicit ec: ExecutionContext): Future[Either[String,JobDefinition]] =
      Future(RU.Jobs.get_def(key))

    def pop (key: String)(implicit ec: ExecutionContext): Future[Either[String,JobDefinition]] =
      Future{
        if (RU.Jobs.has_schedule(key)) RU.Schedules.stop_schedule(key)
        if (RU.Jobs.has_exectime(key)) RU.Schedules.pop_exectimes(key)
        RU.Jobs.pop_def(key)
      }

    def make (jbdef: JobDefinition)(implicit ec: ExecutionContext): Future[Job] =
      Future(make_job(jbdef))

    def list_def ()(implicit ec: ExecutionContext): Future[Either[String,List[String]]] =
      Future(RU.Jobs.list_def())

    def get_status (job: String)(implicit ec: ExecutionContext): Future[Either[String,JobInstanceStatus]] =
      Future(RU.Jobs.get_status(job))

    def list_running ()(implicit ec: ExecutionContext): Future[Either[String,List[String]]] =
      Future(RU.Jobs.list_running())

    def get_outcome (job: String)(implicit ec: ExecutionContext): Future[Either[String,JobPayload]] =
      Future(RU.Jobs.get_outcome(job))

    def list_outcomes ()(implicit ec: ExecutionContext): Future[Either[String,List[String]]] =
      Future(RU.Jobs.list_outcomes())

    def exec (jb: Job)(implicit ec: ExecutionContext): Future[Unit] = 
      Future {
        val hash = make_job_hash(jb)
        val et = jb.exec_times.times.head
        val tz = jb.job_def.schedule.tz
        val now = zoned_instant(tz)
        RU.Jobs.register_run(hash)
        RU.Jobs.register_outcome(hash)
        make_job_directory(hash) match {
          case Success(h) =>
            tell_to(Info(s"Job ${h} starting"), "slik")
          case Failure(ex) =>
            tell_to(Err(s"Job ${hash} cannot run, ${ex.getMessage}"), "slik")
        }
        val env = Map("JBHASH" -> hash, "EXECTIME" -> et.toString,
          "STARTTIME" ->  now.toString)
        for (t <- jb.job_def.workflow) tell_to(ExecTask(hash, t, env), "wrkr")
        tell_to(FinalizeJob(hash), "wrkr")
      }

    def finalize (hash: String)(implicit ec: ExecutionContext): Future[Unit] = {
      Future {
        lazy val f = Future {
          while (!RU.Jobs.is_finished(hash)) {
            Thread.sleep(1000L)
          }
          RU.Jobs.finalize(hash)
          clean_job_directory(hash) match {
            case Success(h) =>
              tell_to(Info(s"clened job directory ${h}"), "slik")
            case Failure(ex) =>
              tell_to(Err(ex.getMessage), "slik")
          }
        }
        Await.ready(f, Duration(2, "days"))
      }
    }
  }

  object KVStore {

    def push (taskarg: TaskArg)(implicit ec: ExecutionContext): Future[Boolean] =
      Future(RU.KVStore.push(taskarg))

    def pop (key: String)(implicit ec: ExecutionContext): Future[Option[TaskArg]] =
      Future(RU.KVStore.pop(key))

    def get (key: String)(implicit ec: ExecutionContext): Future[Option[TaskArg]] =
      Future(RU.KVStore.get(key))

    def list ()(implicit ec: ExecutionContext): Future[Either[String,List[String]]] =
      Future(RU.KVStore.list())

  }

  object Schedules {
    def register_exectimes (name: String, et: ExecutionTimes)(implicit ec: ExecutionContext): Future[Unit] =
      Future(RU.Schedules.register_exectimes(name, et))

    def get_exectimes (name: String)(implicit ec: ExecutionContext): Future[Either[String,ExecutionTimes]] =
      Future(RU.Schedules.get_exectimes(name))

    def stop (name: String)(implicit ec: ExecutionContext): Future[Either[String, String]] =
      Future(RU.Schedules.stop_schedule(name))

    def pop (name: String)(implicit ec: ExecutionContext): Future[Either[String,ExecutionTimes]] =
      Future {
        if (RU.Jobs.has_schedule(name)) RU.Schedules.stop_schedule(name)
        RU.Schedules.pop_exectimes(name)
      }

    def list_exectimes ()(implicit ec: ExecutionContext): Future[Either[String,List[String]]] =
      Future(RU.Schedules.list_exectimes())

    def schedule_job (jb: Job)(implicit ec: ExecutionContext): Future[Job] = {
      Future{
        lazy val schd = Future {
          tell_to(RegisterExecTimes(jb.job_def.name, jb.exec_times), "wrkr")
          val tz = jb.job_def.schedule.tz
          val et = jb.exec_times.times.head
          tell_to(
            Info(s"Next run for job ${jb.job_def.name} scheduled at ${et.toString}"),
            "slik"
          )
          while (zoned_instant(tz).withNano(0) != et) {
            Thread.sleep(100L)
          }
          tell_to(
            Info(s"Job ${jb.job_def.name} will run asap"),
            "slik"
          )
          tell_to(ExecJob(jb), "wrkr")
          make_next_job(jb)
        }
        jb.job_def.schedule.frequency match {
          case "daily" => Await.result(schd, Duration(2, "days"))
          case "weekly" => Await.result(schd, Duration(10, "days"))
          case _ => throw new Exception("Schedule frequency not implemented")
        }
      }
    }
  }

  object Tasks {

    def get_status (job: String, task: String)(implicit ec: ExecutionContext): Future[Either[String,TaskInstanceStatus]] =
      Future(RU.Tasks.get_status(job, task))

    def list_running (job: String)(implicit ec: ExecutionContext): Future[Either[String,List[String]]] =
      Future(RU.Tasks.list_running(job))

    def get_outcome (job: String, name: String)(implicit ec: ExecutionContext): Future[Either[String,TaskPayload]] =
      Future(RU.Tasks.get_outcome(job, name, true))

    def list_outcomes (job: String)(implicit ec: ExecutionContext): Future[Either[String,List[String]]] =
      Future(RU.Tasks.list_outcomes(job))

    def update_status (hash: String, name: String, status: String)(implicit ec: ExecutionContext): Future[Unit] =
      Future(RU.Tasks.update_status(hash, name, status))

    def finalize (hash: String, name: String, outcome: Try[String])(implicit ec: ExecutionContext): Future[Unit] =
      Future(RU.Tasks.finalize(hash, name, outcome))

    def cancel (hash: String, name: String)(implicit ec: ExecutionContext): Future[Either[String,String]] =
      Future(RU.Tasks.cancel(hash, name))

    def exec (hash: String, task: MainTask, env: Map[String, String])(implicit ec: ExecutionContext): Future[Unit] = {
      Future {
        lazy val f = Future{
          RU.Tasks.update_status(hash, task.name, Statuses.RUNNING)
          val ans  = exec_task(hash, task, env)
          ans match {
            case Success(out) => {
              RU.Tasks.update_status(hash, task.name, Statuses.SUCCESS)
            }
            case Failure(ex) => {
              tell_to(Err(ex.getMessage), "slik")
              RU.Tasks.update_status(hash, task.name, Statuses.FAILED)
            }
          }
          ans
        }
        val dep_outcomes = RU.Tasks.wait(hash, task.deps)
        if (dep_outcomes.forall(_ == Statuses.SUCCESS)) {
          val to = Duration(task.timeout, "minutes")
          RU.Tasks.complete_with(hash, task.name, f)
          val outcome = Await.result(f, to)
          tell_to(TaskOutcome(hash, task.name, outcome), "wrkr")
        } else {
          RU.Tasks.update_status(hash, task.name, Statuses.BLOCKED)
        }
      }
    }
  }
}
