package slikdag

import scala.sys.process.ProcessBuilder
import slikdag.Tasks.Task


object Directives {

  sealed trait Directive

  case class DO (val p: ProcessBuilder) extends Directive

  case class GOTO (val task: String) extends Directive

  val _IF = "(_IF_.*)".r
  val _SWITCH = "(_SWITCH_.*)".r
  val _DO = "(_DO_.*)".r
  val _GOTO = "(_GOTO_.*)".r

  object Operators {

    val _then = "(_then_.*)".r
    val _pipe = "(_pipe_.*)".r
    val _and = "(_and_.*)".r
    val _or = "(_or_.*)".r
    val _eqi = "(_eqi_.*)".r
    val _eqs = "(_eqs_.*)".r
    val _nei = "(_nei_.*)".r
    val _nes = "(_nes_.*)".r
    val _gti = "(_gti_.*)".r
    val _gts = "(_gts_.*)".r
    val _gtf = "(_gtf_.*)".r
    val _lti = "(_lti_.*)".r
    val _ltf = "(_ltf_.*)".r
    val _gei = "(_gei_.*)".r
    val _gef = "(_gef_.*)".r
    val _lei = "(_lei_.*)".r
    val _lef = "(_lef_.*)".r
    val _bool = "(_bool_.*)".r
    val _sh = "(_sh_.*)".r
    val _bash = "(_bash_.*)".r

  }
}

object DirectiveParsers {

  import java.io.File

  import scala.util.{Try, Success, Failure}
  import scala.sys.process.Process
  //import scala.reflect.runtime.universe._

  import io.circe.{ACursor, Decoder}
  import io.circe.parser.decode

  import slikdag.Directives._
  import slikdag.Directives.Operators._

  type DirEnv = List[(String, String)]

  def _parse (key: String, x: ACursor, wf: File, env: List[(String, String)]): Directive =
    key match {
      case _IF(k) => If(x, wf, env)
      case _SWITCH(k) => Switch(x, wf, env)
      case _DO(k) => Do(x, wf, env)
      case _GOTO(k) => Goto(x)
      case _ => throw new Exception(s"${key} is not a legal directive.")
    }

  def _parse (x: ACursor, wf: File, env: List[(String, String)]): Directive =
    x.keys match {
      case Some(k) => {
        if (k.toList.length == 1) _parse(k.toList.head, x.downField(k.toList.head), wf, env)
        else throw new Exception("More than one directive")
      }
      case _ => throw new Exception("No directives found")
    }

  def parse (x: ACursor, wf: File, env: DirEnv): Try[List[Directive]] =
    Try {
      val keys = x.keys match {
        case Some(k) => k.toList
        case _ => throw new Exception("No directives found")
      }
      keys.map((k: String) => _parse(k, x.downField(k), wf, env))
    }

  object If {

    def apply (x: ACursor, wf: File, env: List[(String, String)]): Directive = {
      val keys = x.keys match {
        case Some(k) => k.toList
        case _ => throw new Exception("__if need keys")
      }
      if (this.parse(keys(0), x.downField(keys(0)))) {
        _parse(keys(1), x.downField(keys(1)), wf, env)
      } else {
        _parse(keys(2), x.downField(keys(2)), wf, env)
      }
    }

    def and (x: ACursor): Boolean = {
      x.keys match {
        case Some(k) => {
          val args = k.toList
          if (args.length != 2) throw new Exception("_and_ operator in _IF_ directive takes two arguments")
          if (this.parse(args(0), x.downField(args(0)))) {
            this.parse(args(1), x.downField(args(1)))
          }else {
            false
          }
        }
        case _ => {
          throw new Exception("_and_ operator in _IF_ directive needs two named arguments ")
        }
      }
    }

    def or (x: ACursor): Boolean = {
      x.keys match {
        case Some(k) => {
          val args = k.toList
          if (args.length != 2) throw new Exception("_or_ operator in _IF_ directive operator takes two arguments")
          if (this.parse(args(0), x.downField(args(0)))) true
          else this.parse(args(1), x.downField(args(1)))
        }
        case _ => throw new Exception("_or_ operator in _IF_ directive needs two named arguments")
      }
    }

    def eq [T <% Ordered[T]: Decoder](x: ACursor): Boolean = {
      x.as[List[T]] match {
        case Right(args) => {
          if (args.length != 2) throw new Exception("_eq_ operator in _IF_ directive takes two arguments")
          args(0) == args(1)
        }
        case Left(ex) => throw new Exception(ex)
      }
    }

    def ne [T <% Ordered[T]: Decoder](x: ACursor): Boolean = {
      x.as[List[T]] match {
        case Right(args) => {
          if (args.length != 2) throw new Exception("_ne_ operator in _IF_ directive takes two arguments")
          args(0) != args(1)
        }
        case Left(ex) => throw new Exception(ex)
      }
    }

    def gt [T <% Ordered[T]: Decoder](x: ACursor): Boolean = {
      x.as[List[T]] match {
        case Right(args) => {
          if (args.length != 2) throw new Exception("_gt_ in _IF_ directive operator takes two arguments")
          args(0) > args(1)
        }
        case Left(ex) => throw new Exception(ex)
      }
    }

    def lt [T <% Ordered[T]: Decoder](x: ACursor): Boolean = {
      x.as[List[T]] match {
        case Right(args) => {
          if (args.length != 2) throw new Exception("_lt_ in _IF_ directive operator takes two arguments")
          args(0) < args(1)
        }
        case Left(ex) => throw new Exception(ex)
      }
    }

    def ge [T <% Ordered[T]: Decoder](x: ACursor): Boolean = {
      x.as[List[T]] match {
        case Right(args) => {
          if (args.length != 2) throw new Exception("_ge_ in _IF_ directive operator takes two arguments")
          args(0) >= args(1)
        }
        case Left(ex) => throw new Exception(ex)
      }
    }

    def le [T <% Ordered[T]: Decoder](x: ACursor): Boolean = {
      x.as[List[T]] match {
        case Right(args) => {
          if (args.length != 2) throw new Exception("_le_ in _IF_ directive operator takes two arguments")
          args(0) <= args(1)
        }
        case Left(ex) => throw new Exception(ex)
      }
    }

    def bool (x: ACursor): Boolean = {
      x.as[Boolean] match {
        case Right(b) => b
        case Left(ex) => throw new Exception(ex)
      }
    }

    def parse (key: String, x: ACursor): Boolean = {
      key match {
        case _and(k) => this.and(x)
        case _or(k) => this.or(x)
        case _eqi(k) => this.eq[Int](x)
        case _eqs(k) => this.eq[String](x)
        case _nei(k) => this.ne[Int](x)
        case _nes(k) => this.ne[String](x)
        case _gti(k) => this.gt[Int](x)
        case _gtf(k) => this.gt[Float](x)
        case _lti(k) => this.lt[Int](x)
        case _ltf(k) => this.lt[Float](x)
        case _gei(k) => this.ge[Int](x)
        case _gef(k) => this.ge[Float](x)
        case _lei(k) => this.le[Int](x)
        case _lef(k) => this.le[Float](x)
        case _bool(k) => this.bool(x)
        case _ => throw new Exception(s"${key} is not a legal operator in _IF_ directive.")
      }
    }
  }

  object Switch {

    def apply (x: ACursor, wf: File, env: List[(String, String)]): Directive = {
      val keys = x.keys match {
        case Some(k) => k.toList
        case _ => throw new Exception("_SWITCH_ directive needs named arguments")
      }
      x.downField(keys.head).as[String] match {
        case Right(key) => {
          val next = x.downField(key)
          _parse(next, wf, env)
        }
        case Left(ex) => throw new Exception(ex)
      }
    }
  }

  object Do {

    def apply (x: ACursor, wf:File, env: List[(String, String)]): DO = 
      x.keys match {
        case Some(k) => {
          if (k.toList.length == 1) DO(this.parse(k.toList.head, x.downField(k.toList.head), wf, env))
          else throw new Exception("_DO_ directive can create only one process")
        }
        case _ => throw new Exception("_DO_ directive require one operator")
      }

    def parse (key: String, x: ACursor, wf: File, env: List[(String, String)]): ProcessBuilder =
      key match {
        case _then(k) => this.compose("_then_", x, wf, env)
        case _and(k) => this.compose("_and_", x, wf, env)
        case _or(k) => this.compose("_or_", x, wf, env)
        case _pipe(k) => this.compose("_pipe_", x, wf, env)
        case _sh(k) => this.sh(x, wf, env)
        case _bash(k) => this.bash(x, wf, env)
        case _ => throw new Exception(s"Unknown key ${key} in _DO_ directive")
      }

    def sh (x: ACursor, wf: File, env: List[(String, String)]): ProcessBuilder =
      x.as[String] match {
        case Right(cmd) => Process(List("sh", "-c", cmd), wf, env:_*)
        case Left(ex) => throw new Exception(ex)
      }

    def bash (x: ACursor, wf: File, env: List[(String, String)]): ProcessBuilder = 
      x.as[String] match {
        case Right(cmd) => Process(List("bash", "-c", cmd), wf, env:_*)
        case Left(ex) => throw new Exception(ex)
      }

    def compose (op: String, x: ACursor, wf: File, env: List[(String, String)]): ProcessBuilder = {
      x.keys match {
        case Some(k) => {
          val args = k.toList
          println(op)
          println(args)
          if (args.length != 2) throw new Exception(
            f"${op} operator in _DO_ directive takes two arguments")
          op match {
            case "_then_" =>
              this.parse(
                args(0), x.downField(args(0)), wf: File, env: List[(String, String)]
              ) ### this.parse(
                args(1), x.downField(args(1)), wf: File, env: List[(String, String)]
              )
            case "_and_" =>
              this.parse(
                args(0), x.downField(args(0)), wf: File, env: List[(String, String)]
              ) #&& this.parse(
                args(1), x.downField(args(1)), wf: File, env: List[(String, String)]
              )
            case "_or_" =>
              this.parse(
                args(0), x.downField(args(0)), wf: File, env: List[(String, String)]
              ) #|| this.parse(
                args(1), x.downField(args(1)), wf: File, env: List[(String, String)]
              )
            case "_pipe_" =>
              this.parse(
                args(0), x.downField(args(0)), wf: File, env: List[(String, String)]
              ) #| this.parse(
                args(1), x.downField(args(1)), wf: File, env: List[(String, String)]
              )
          }
        }
        case _ =>
          throw new Exception(s"${op} in _DO_ directive needs two named arguments")
      }
    }
  }

  object Goto {

    def apply (x: ACursor): GOTO = 
      x.as[String] match {
        case Right(task) => 
          GOTO(task)
        case Left(ex) => throw new Exception(ex)
      }
  }

}
