package slikdag

object Tasks {

  import io.circe.{Decoder, Encoder, Json}
  import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

  import akka.actor.typed.ActorRef

  import scala.concurrent.Promise
  import scala.util.Try

  import slikdag.Messages.Message

  sealed trait Task {
    val name: String
    val dirs: Json
    val timeout: Int
    val workstream: Workstream
  }

  case class SubTask (name: String, dirs: Json, timeout:Int, workstream: Workstream) extends Task
  type Workstream = List[SubTask]
  implicit val SubTaskDecoder: Decoder[SubTask] = deriveDecoder[SubTask]
  implicit val SubTaskEncoder: Encoder[SubTask] = deriveEncoder[SubTask]

  case class MainTask (name: String, deps: List[String], dirs: Json, timeout: Int, workstream: Workstream) extends Task
  type Workflow = List[MainTask]

  implicit val MainTaskDecoder: Decoder[MainTask] = deriveDecoder[MainTask]
  implicit val MainTaskEncoder: Encoder[MainTask] = deriveEncoder[MainTask]

  case class TaskPayload(name: String, outcome: String, info: String)

  implicit val TaskPayloadEncoder: Encoder[TaskPayload] = deriveEncoder[TaskPayload]

  class TaskInstance (var status: String, var hasFuture: Boolean, val runner: ActorRef[Message],
      val promise: Promise[Try[String]])

  case class TaskInstanceStatus (name: String, status: String, hasFuture: Boolean)

  implicit val TaskInstanceStatusEncoder: Encoder[TaskInstanceStatus] = deriveEncoder[TaskInstanceStatus]

  case class TaskArg (key: String, dtype: String, value: String)

  implicit val TaskArgDecoder: Decoder[TaskArg] = deriveDecoder[TaskArg]
  implicit val TaskArgEncoder: Encoder[TaskArg] = deriveEncoder[TaskArg]

  object Statuses {

    val SUCCESS = "SUCCESS"
    val FAILED = "FALIED"
    val PENDING = "PENDING"
    val RUNNING = "RUNNING"
    val BLOCKED = "BLOCKED"
    val CANCELLED = "CANCELLED"
    val TERMINATED = List(
      SUCCESS,
      FAILED,
      BLOCKED,
      CANCELLED
    )
    val ALL = List(
      SUCCESS,
      FAILED,
      PENDING,
      RUNNING,
      BLOCKED,
      CANCELLED
    )

  }


}

object TasksUtils {

  import scala.sys.process._
  import scala.util.{Try, Success, Failure}
  import scala.reflect.io.Directory

  import io.circe.ACursor
  import io.circe.yaml.parser
 
  import java.io.File

  import slikdag.Tasks._
  import slikdag.DirectiveParsers.parse
  import slikdag.Directives._
  import slikdag.RegistersUtils.KVStore.get

  val _key = """\$\{([A-Za-z0-9_\./]+)\}""".r

  def _format_dirs(task: Task, env: Map[String, String]): ACursor = {
    val keys = _key.findAllMatchIn(task.dirs.toString)
    val values = keys.map(_ match {
      case _key(k) => if (env.contains(k)) env(k) else get(k) match {
        case Some(arg) => arg.value
        case None => throw new Exception(s"Unknown argument ${k} in task ${task.name}")
      }
      case _ => throw new Exception(s"Unknown match in task ${task.name}")
    }).toList
    val json = parser.parse(_key.replaceAllIn(task.dirs.toString, "%s").
      format(values:_*))
    json match {
      case Right(j) => j.hcursor
      case Left(ex) => throw new Exception(
        s"${ex.getMessage} in formatting directive for task ${task.name}"
      )
    }
  }

  def _exec_process(p: ProcessBuilder): String = {
    val buffer = new StringBuffer()
    buffer.append("\n")
    p ! ProcessLogger(e => buffer.append(e +"\n"), o => buffer.append(o + "\n"))
    buffer.toString
  }

  def _exec_task(task: Task, wf:File, env: Map[String, String], lv: Int): String = {
    parse(_format_dirs(task, env), wf, env.toList) match {
      case Success(dirs) => {
        val ans = dirs.map((x: Directive) => {
          val ansx = x match {
            case DO(p) => {
              s"```${_exec_process(p)}```"
            }
            case GOTO(t) => {
              val nexts = task.workstream.filter((x: Task) => x.name == t)
              if (nexts.length > 1) {
                throw new Exception(s"More than one match for _GOTO_ directive in task ${task.name}")
              } else {
                _exec_task(nexts.head, wf, env, lv + 1)
              }
            }
            case _ => throw new Exception(s"Unknown directive ${x} in task ${task.name}")
          }
          s"""${"\t" * (lv - 1)}+ **${x.toString}**\n${ansx}"""
        }).mkString("\n")
        s"""${"#" * lv} ${task.name}\n${ans}"""
      }
      case Failure(ex) => {
        throw new Exception(s"${ex.getMessage} in task ${task.name}")
      }
    }
  }


  def exec_task (hash: String, task: Task, env: Map[String, String]): Try[String] = {
    Try {
      if (env.isEmpty) throw new Exception(s"Task ${task.name} cannot run, empty environment")
      val wf = new File(hash)
      if (wf.exists) {
        _exec_task(task, wf, env, 1)
      } else {
        throw new Exception(
          s"Task ${task.name} cannot run, job directory ${hash} not found."
        )
      }
    }
  }
}

