package slikdag

object Schedules {

  import io.circe.{Decoder, Encoder}
  import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

  import java.time.ZonedDateTime

  case class Schedule (tz: String, frequency:String, when: List[String])

  implicit val ScheduleDecoder: Decoder[Schedule] = deriveDecoder[Schedule]
  implicit val ScheduleEncoder: Encoder[Schedule] = deriveEncoder[Schedule]

  case class ExecutionTimes (frequency: String, times: List[ZonedDateTime])

  implicit val ExecutionTimesEncoder: Encoder[ExecutionTimes] = deriveEncoder[ExecutionTimes]

}

object SchedulesUtils {

  import java.time._

  import Schedules.{Schedule, ExecutionTimes}

  val WEEK_DAYS = Map(
    "Monday" -> DayOfWeek.MONDAY,
    "Tuesday" -> DayOfWeek.TUESDAY,
    "Wednesday" -> DayOfWeek.WEDNESDAY,
    "Thursday" -> DayOfWeek.THURSDAY,
    "Friday" -> DayOfWeek.FRIDAY,
    "Saturday" -> DayOfWeek.SATURDAY,
    "Sunday" -> DayOfWeek.SUNDAY
  )

  def execution_times (sc: Schedule): ExecutionTimes = {
    val zinstant = zoned_instant(sc.tz)
    sc.frequency match {
      case "hourly" => println("to implement"); ExecutionTimes(sc.frequency, Nil)
      case "daily" =>
        ExecutionTimes(sc.frequency, sc.when.map(_daily_init_execution_time(_, zinstant)).sorted)
      case "weekly" =>
        ExecutionTimes(sc.frequency, sc.when.map(_weekly_init_execution_time(_, zinstant)).sorted)
      case "monthly" => println("to implement"); ExecutionTimes(sc.frequency, Nil)
      case "yearly" => println("to implement"); ExecutionTimes(sc.frequency, Nil)
      case "adhoc" => println("to implement"); ExecutionTimes(sc.frequency, Nil)
    }
  }

  // def next_execution_times (execution_times: ExecutionTimes): ExecutionTimes = {
  //   execution_times.frequency match {
  //     case "daily" => {
  //       val new_times = execution_times.times.tail :+ execution_times.times.head.plusDays(1)
  //       ExecutionTimes(execution_times.frequency, new_times)
  //     }
  //     case _ => ExecutionTimes(execution_times.frequency, Nil)
  //   }
  // }

  def zoned_instant (tz: String): ZonedDateTime =
    Instant.now.atZone(ZoneId.of(tz))

  def _daily_init_execution_time (x: String, zinstant: ZonedDateTime): ZonedDateTime = {
    val sc_time = LocalTime.parse(x)
    val sc_datetime = zinstant.withHour(sc_time.getHour()).
      withMinute(sc_time.getMinute()).
      withSecond(sc_time.getSecond()).
      withNano(0)
    if (sc_datetime.isAfter(zinstant)) sc_datetime else sc_datetime.plusDays(1)
  }

  def _weekly_init_execution_time (x: String, zinstant: ZonedDateTime): ZonedDateTime = {
    val sc_parts = x.split("@")
    val sc_time = LocalTime.parse(sc_parts(1))
    val sc_datetime =  zinstant.withHour(sc_time.getHour()).
      withMinute(sc_time.getMinute()).
      withSecond(sc_time.getSecond())
    val d = WEEK_DAYS(sc_parts(0)).getValue - zinstant.getDayOfWeek().getValue()
    if (d > 0) sc_datetime.plusDays(d) else sc_datetime.plusDays(d + 7)
  }
  
}
