package slikdag



object Agents {

  import scala.util.{Success, Failure}
  
  import scala.sys.process._
  import scala.concurrent.{Future, Promise}
  //import scala.concurrent.ExecutionContext.Implicits.global
  import scala.util.{Try, Success, Failure}

  import akka.actor.typed.{Behavior, SupervisorStrategy, PostStop}
  import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext}
  import akka.actor.typed.scaladsl.{Behaviors, Routers}

  import slikdag.Tasks.{TaskInstance, Statuses}
  import slikdag.Messages._
  import slikdag.SchedulesUtils.zoned_instant
  import slikdag.JobsUtils.{make_job, make_next_job, make_job_hash}
  import slikdag.RegistersUtils.tell_to
  import slikdag.{RegistersUtils => RU}
  import slikdag.{Actions => A}
  import slikdag.Registers._

  import akka.actor.typed.ActorSystem

  val slik = ActorSystem[Message](SlikdagManager(), "SlikDag")

  implicit val executionContext = slik.executionContext

  object SlikdagManager {

    def apply (): Behavior[Message] = act()

    private def act (): Behavior[Message] = {
      Behaviors.receive(
        (context: ActorContext[Message], msg: Message) => {
          msg match {
            case Start() => {
              context.log.info("SlikDag starting....")
              create_env(context)
              context.log.info("SlikDag STARTED")
            }
            case Stop() => {
              context.log.info("SlikDag, STOPPED")
              Behaviors.stopped[Message]
            }
            case Info(msg) =>
              context.log.info(msg)
            case Err(msg) =>
              context.log.error(msg)
            case ReadJobDef(jbdef, ans) =>
              ans.completeWith(A.Jobs.read_def(jbdef))
            case MakeJob(jbdef, ans) =>
              ans.completeWith(A.Jobs.make(jbdef))
            case RegisterJobDef(jbde) =>
              tell_to(msg, "wrkr")
            case ScheduleJob(jb) =>
              tell_to(msg, "wrkr")
            case GetJobDef(key, ans) =>
              ans.completeWith(A.Jobs.get_def(key))
            case PopJobDef(key, ans) =>
              ans.completeWith(A.Jobs.pop(key))
            case ListJobDefs(ans) =>
              ans.completeWith(A.Jobs.list_def())
            case RegisterExecTimes(key, et) =>
              tell_to(msg, "wrkr")
            case GetExecTimes(key, ans) =>
              ans.completeWith(A.Schedules.get_exectimes(key))
            case PopExecTimes(key, ans) =>
              ans.completeWith(A.Schedules.pop(key))
            case StopSchedule(key, ans) =>
              ans.completeWith(A.Schedules.stop(key))
            case ListExecTimes(ans) =>
              ans.completeWith(A.Schedules.list_exectimes())
            case GetTaskInstanceStatus(job, task, ans) =>
              ans.completeWith(A.Tasks.get_status(job, task))
            case GetJobInstanceStatus(job, ans) =>
              ans.completeWith(A.Jobs.get_status(job))
            case ListRunning(ans) =>
              ans.completeWith(A.Jobs.list_running())
            case ListRunningJob(job, ans) =>
              ans.completeWith(A.Tasks.list_running(job))
            case GetTaskOutcome(job, task, ans) =>
              ans.completeWith(A.Tasks.get_outcome(job, task))
            case GetJobOutcome(job, ans) =>
              ans.completeWith(A.Jobs.get_outcome(job))
            case ListOutcomes(ans) =>
              ans.completeWith(A.Jobs.list_outcomes())
            case ListJobOutcome(job, ans) =>
              ans.completeWith(A.Tasks.list_outcomes(job))
            case AppendTaskArg(arg, ans) =>
              ans.completeWith(A.KVStore.push(arg))
            case PopTaskArg(key, ans) => 
              ans.completeWith(A.KVStore.pop(key))
            case GetTaskArg(key, ans) => 
              ans.completeWith(A.KVStore.get(key))
            case ListArgs(ans) =>
              ans.completeWith(A.KVStore.list())
            case CancelTask(hash, name, ans) =>
              ans.completeWith(A.Tasks.cancel(hash, name))
            case _ => context.log.error("Slik Unknown message")  
          }
          Behaviors.same[Message]
        }
      )
    }

    private def create_env (context: ActorContext[Message]): Unit = {
      import slikdag.Registers.ENV

      if (ENV.size > 0) {
        throw new Exception("ENV already created.")
      } else {
        ENV.put("slik", context.self)
        ENV.put("wrkr", context.spawn(WorkerRouter, "WorkerRouter"))
      }
    }
  }  

  object Worker {

    def apply (): Behavior[Message] = act()

    private def act (): Behavior[Message] = {
      Behaviors.receive(
        (context: ActorContext[Message], msg: Message) => {
          msg match {
            case RegisterJobDef(jbdef) =>
              A.Jobs.register_def(jbdef)
            case ScheduleJob(jb) =>  {
              if (RU.Jobs.has_schedule(jb.job_def.name))
                RU.Schedules.stop_schedule(jb.job_def.name)
              val scheduler = context.spawn(Runner(), s"${jb.job_def.name}_scheduler")
                RU.Schedules.register_schedule(jb.job_def.name, scheduler)
                scheduler ! msg
            }
            case ExecJob(jb) => 
              A.Jobs.exec(jb)
            case ExecTask(hash, task, env) =>
              val runner = context.spawn(Runner(), s"${hash}-${task.name}_runner")
              val instance = new TaskInstance(Statuses.PENDING, false, runner, Promise[Try[String]])
              RU.Tasks.register_instance(hash, task.name, instance)
              runner ! msg
            case TaskStatus(hash, name, status) => 
              A.Tasks.update_status(hash, name, status)
            case TaskOutcome(hash, name, outcome) => 
              A.Tasks.finalize(hash, name, outcome)
            case RegisterExecTimes(key, et) =>
              A.Schedules.register_exectimes(key, et)
            case FinalizeJob(hash) =>
              A.Jobs.finalize(hash)
            case _ => context.log.info(s"Unknown message ${msg}")
          }
          Behaviors.same[Message]
        }
      )
    }
  }

  val WorkerRouter = Routers.pool(8)(Behaviors.supervise(Worker())
    .onFailure[Exception](SupervisorStrategy.restart))
    .withRoundRobinRouting()

  object Runner {

    def apply (): Behavior[Message] = act()

    private def act (): Behavior[Message] = {
      Behaviors.receive[Message](
        (context: ActorContext[Message], msg: Message) => {
          msg match {
            case Stop() =>
              Behaviors.stopped
            case ScheduleJob(jb) =>
              A.Schedules.schedule_job(jb) onComplete {
                case Success(next) =>
                  context.self ! ScheduleJob(next)
                case Failure(ex) =>
                  tell_to(Err(s"Schedule of job ${jb.job_def.name} failed with ${ex.getMessage}."), "slik")
                  context.self ! Stop()
              }
              Behaviors.same[Message]
            case ExecTask(hash, task, env) =>
              A.Tasks.exec(hash,task, env)
              Behaviors.same[Message]
            case _ => 
              tell_to(Err(s"Unknown message ${msg}"), "slik")
              Behaviors.same[Message]
          }
        }
      ).receiveSignal {
        case (context, PostStop) =>
          tell_to(Info(s"${context.self.path.name} terminated"), "slik")
          Behaviors.same
        case _ => Behaviors.same
      }
    }
  }

}
