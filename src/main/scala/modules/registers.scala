package slikdag

object Registers {

  import scala.collection.mutable.HashMap
  import scala.concurrent.Future
  //import scala.util.Try

  import akka.actor.typed.ActorRef

  import slikdag.Messages.Message
  import slikdag.Tasks.{Workflow, TaskPayload, TaskInstance, TaskArg}
  import slikdag.Schedules.ExecutionTimes
  import slikdag.Jobs.JobDefinition

  val ENV = new HashMap[String, ActorRef[Message]]

  val JOBDEFS = new HashMap[String, JobDefinition]()

  val SCHEDULES = new HashMap[String, ActorRef[Message]]()

  val EXECTIMES = new HashMap[String, ExecutionTimes]()

  val RUNNING = new HashMap[String, HashMap[String, TaskInstance]]()

  val OUTCOMES = new HashMap[String, HashMap[String, TaskPayload]]()

  val ARGS = new HashMap[String, TaskArg]

}

object RegistersUtils {

  import scala.collection.mutable.HashMap
  import scala.concurrent.{Future, Promise}
  import scala.util.{Either, Right, Left, Try, Success, Failure}

  import akka.actor.typed.ActorRef
  import akka.actor.typed.scaladsl.ActorContext
 

  import slikdag.Registers._
  import slikdag.Messages._
  import slikdag.Tasks.{MainTask, Workflow, TaskPayload, TaskInstance, TaskInstanceStatus, TaskArg, Statuses}
  import slikdag.Jobs.{JobDefinition, JobInstanceStatus, JobPayload}
  import slikdag.Schedules.ExecutionTimes

  def tell_to (msg: Message, act: String): Unit = {
    act match {
      case "slik" => ENV("slik") ! msg
      case "wrkr" => ENV("wrkr") ! msg
      case _ => tell_to(Err(s"Message addressed to unknown agent ${act}"), "slik")
    }
  }

  object Jobs {

    def register_def (jbdef: JobDefinition): Unit = {
      JOBDEFS.put(jbdef.name, jbdef)
      tell_to(
        Info(s"Registered new job definition as SCHEDULES(${jbdef.name})"),
        "slik")
    }

    def get_def (name: String): Either[String,JobDefinition] = {
      if (JOBDEFS.contains(name)) {
        tell_to(Info(s"Retrieved ${name} from JOBDEFS"), "slik")
        Right(JOBDEFS(name))
      } else {
        val msg = s"${name} not present in JOBDEFS"
        tell_to(Err(msg), "slik")
        Left(msg)
      }
    }

    def pop_def (key: String): Either[String,JobDefinition] = {
      JOBDEFS.remove(key) match {
        case Some(jd) => 
          Right(jd)
        case None => {
          val msg = s"Impossible to pop job ${key}, not found"
          tell_to(Err(msg), "slik")
          Left(msg)
        }
      }
    }

    def list_def (): Either[String,List[String]] = {
      if (JOBDEFS.isEmpty) {
        val msg = "JOBDEFS is empty, nothing to list"
        tell_to(Err(msg), "slik")
        Left(msg)
      } else {
        tell_to(Info("Listing JOBDEFS keys"), "slik")
        Right(JOBDEFS.keys.toList)
      }
    }

    def register_run(hash: String): Unit = {
      if (RUNNING.contains(hash)) {
        tell_to(Err(s"Run ${hash} already in  RUNNING"), "slik")
      } else {
        val newrun = new HashMap[String, TaskInstance]()
        RUNNING.put(hash, newrun)
        tell_to(Info(s"Appended new run at RUNNING(${hash})"), "slik")
      }
    }

    def register_outcome(hash: String): Unit = {
      if (OUTCOMES.contains(hash)) {
        tell_to(Err(s"${hash} already in OUTCOMES"), "slik")
      } else {
        OUTCOMES.put(hash, new HashMap[String, TaskPayload])
        tell_to(Info(s"${hash} registered in OUTCOMES"), "slik")
      }
    }

    def list_running (): Either[String, List[String]] = {
      if (RUNNING.isEmpty) {
        val msg = "RUNNING is empty, nothing to list"
        tell_to(Err(msg), "slik")
        Left(msg)
      } else {
        tell_to(Info("Listing RUNNING keys"), "slik")
        Right(RUNNING.keys.toList)
      }
    }

    def get_status (job: String): Either[String,JobInstanceStatus] = {
      if (RUNNING.contains(job)) {
        val statuses = RUNNING(job).map[TaskInstanceStatus]((x: (String, TaskInstance)) => {
          TaskInstanceStatus(x._1, x._2.status, x._2.hasFuture)
        }).toList
        Right(JobInstanceStatus(job, statuses))
      } else {
        val msg = s"Job ${job} not found in RUNNING"
        tell_to(Err(msg), "slik")
        Left(msg)
      }
    }

    def get_outcome(hash: String): Either[String,JobPayload] = {
      if (OUTCOMES.contains(hash)) {
        tell_to(Info(s"Job outcome ${hash} retrieved"), "slik")
        Right(JobPayload(hash, OUTCOMES(hash).values.toList))
      } else {
        val msg = s"${hash} not found in OUTCOMES"
        tell_to(Err(msg), "slik")
        Left(msg)
      }
    }

    def list_outcomes (): Either[String, List[String]] = {
      if (OUTCOMES.isEmpty) {
        val msg = "OUTCOMES is empty, nothing to list"
        tell_to(Err(msg), "slik")
        Left(msg)
      } else {
        tell_to(Info("Listing OUTCOMES keys"), "slik")
        Right(OUTCOMES.keys.toList)
      }
    }

    def is_finished (hash: String): Boolean = {
      if (RUNNING.contains(hash)) {
        if (RUNNING(hash).isEmpty) false
        else RUNNING(hash).forall((x: (String, TaskInstance)) =>
          Statuses.TERMINATED.contains(x._2.status))
      } else {
        tell_to(Err(s"impossible to check if job ${hash} is finished, not found in RUNNING."), "slik")
        true
      }
    }

    def finalize (hash: String): Unit = {
      if (RUNNING.contains(hash)) {
        if (this.is_finished(hash)) {
          val r = RUNNING.remove(hash)
          tell_to(Info(s"finalized job ${hash}"), "slik")
        } else {
          tell_to(Err(s"job ${hash} cannot be finalized, it is still running."), "slik")
        }
      } else {
        tell_to(Err(s"impossible to finalize ${hash}, not found in RUNNING."), "slik")
      }
    }

    def exists (name: String): Boolean =
      JOBDEFS.contains(name)

    def has_schedule (name: String): Boolean =
      SCHEDULES.contains(name) 

    def has_exectime (name: String): Boolean =
      EXECTIMES.contains(name)

    def cancel (hash: String): Either[String,String] = {
      RUNNING.get(hash) match {
        case Some(j) => {
          val ans = j.keys.map(Tasks.cancel(hash, _) match {
            case Right(msg) => msg
            case Left(msg) => msg
          })
          Right(ans.mkString("\n"))
        }
        case None => {
          val msg = s"Impossible to cancel job ${hash}, not found"
          tell_to(Err(msg), "slik")
          Left(msg)
        }
      }
    }
  }

  object Schedules {

    def register_schedule (name: String, schd: ActorRef[Message]): Unit = {
      SCHEDULES.put(name, schd)
      tell_to(Info(s"Registered new schedule as SCHEDULES(${name})"), "slik")
    }

    def register_exectimes (name: String, et: ExecutionTimes): Unit = {
      EXECTIMES.put(name, et)
      tell_to(Info(s"Registered new execution times at EXECTIMES(${name})"), "slik")
    }

    def get_exectimes (name: String): Either[String,ExecutionTimes] = {
      if (EXECTIMES.contains(name)) {
        tell_to(Info(s"Retrieved ${name} from EXECTIMES"), "slik")
        Right(EXECTIMES(name))
      } else {
        val msg = s"${name} not present in EXECTIMES"
        tell_to(Err(msg), "slik")
        Left(msg)
      }
    }

    def stop_schedule (key: String): Either[String, String] = {
      SCHEDULES.remove(key) match {
        case Some(s) => {
          val msg = s"Stopped Schedule ${key} popped"
          tell_to(Info(msg), "slik")
          s ! Stop()
          Right(msg)
        }
        case None => {
          val msg = s"Impossible to stop schedule ${key}, not found"
          tell_to(Err(msg), "slik")
          Left(msg)
        }
      }
    }

    def pop_exectimes(key: String): Either[String,ExecutionTimes] = {
      if (EXECTIMES.contains(key)) {
        tell_to(Info(s"Popped execution times ${key}"), "slik")
        Right(EXECTIMES.remove(key).get)
      } else {
        val msg = s"Impossible to pop execution times ${key} not found"
        tell_to(Err(msg), "slik")
        Left(msg)
      }
    }

    def list_exectimes(): Either[String,List[String]] = {
      if (EXECTIMES.isEmpty) {
        val msg = "EXECTIMES is empty, nothing to list"
        tell_to(Err(msg), "slik")
        Left(msg)
      } else {
        tell_to(Info("Listing EXECTIMES keys"), "slik")
        Right(EXECTIMES.keys.toList)
      }
    }

  }

  object Tasks {

    def register_instance (hash: String, task: String, instance: TaskInstance): Unit = 
      RUNNING(hash).put(task, instance)

    def complete_with(hash: String, task: String, f: Future[Try[String]]): Unit = {
      RUNNING(hash)(task).promise.completeWith(f)
      RUNNING(hash)(task).hasFuture = true
    }

    def list_running (job: String): Either[String,List[String]] = {
      if (RUNNING.contains(job)) {
        if (RUNNING(job).isEmpty) {
          val msg = s"RUNNING(${job}) is empty, nothing to list"
          tell_to(Err(msg), "slik")
          Left(msg)
        } else {
          tell_to(Info("Listing RUNNING(${job}) keys"), "slik")
          Right(RUNNING(job).keys.toList)
        }
      } else {
        val msg = s"${job} not found in RUNNING"
        tell_to(Err(msg), "slik")
        Left(msg)
      }
    }

    def register_runner(hash: String, name: String, f: Future[Try[String]]): Unit = {
      if (RUNNING(hash)(name).hasFuture) {
        tell_to(Err(s"RUNNING(${hash})(${name}) has already assigned a Future."), "slik")
      } else {
        RUNNING(hash)(name).promise.completeWith(f)
        RUNNING(hash)(name).hasFuture = true
        tell_to(Info(s"Future assigned to RUNNING(${hash})(${name})"), "slik")
      }
    }

    def update_status(hash: String, name: String, status: String): Unit = {
      if (RUNNING(hash).contains(name)) {
        if (Statuses.ALL.contains(status)) {
          val oldstatus = RUNNING(hash)(name).status
          RUNNING(hash)(name).status = status
          tell_to(Info(s"Task ${name}@${hash} status updated ${oldstatus} -> ${status}"), "slik")
        } else {
          tell_to(Err(s"Task ${name}@${hash} unknown status ${status}"), "slik")
        }
      } else {
        tell_to(Err(s"Task ${name} not in RUNNING(${hash})"), "slik")
      }
    }

    def get_status (job:String, task: String): Either[String,TaskInstanceStatus] = {
      if (RUNNING.contains(job)) {
        if (RUNNING(job).contains(task)) {
          Right(TaskInstanceStatus(task, RUNNING(job)(task).status, RUNNING(job)(task).hasFuture))
        } else {
          val msg = s"Task ${task} not found in running job ${job}"
          tell_to(Err(msg), "slik")
          Left(msg)
        }
      } else {
        val msg = s"Job ${job} not found in RUNNING"
        tell_to(Err(msg), "slik")
        Left(msg)
      }
    }

    def list_outcomes (job: String): Either[String,List[String]] = {
      if (OUTCOMES.contains(job)) {
        if (OUTCOMES(job).isEmpty) {
          val msg = s"OUTCOMES(${job}) is empty, nothing to list"
          tell_to(Err(msg), "slik")
          Left(msg)
        } else {
          tell_to(Info("Listing OUTCOMES(${job}) keys"), "slik")
          Right(OUTCOMES(job).keys.toList)
        }
      } else {
        val msg = s"${job} not found in RUNNING"
        tell_to(Err(msg), "slik")
        Left(msg)
      }
    }

    def get_outcome (hash: String, name: String, log: Boolean):
        Either[String,TaskPayload] = {
      if (OUTCOMES.contains(hash)) {
        if (OUTCOMES(hash).contains(name)) {
        if (log) tell_to(Info(s"task outcome ${hash}::${name} retrieved"), "slik")
          Right(OUTCOMES(hash)(name))
        } else {
          val msg = s"task ${name} not in ${hash}"
          if (log) tell_to(Err(msg), "slik")
          Left(msg)
        }
      } else {
      val msg = s"${hash} not found"
        if (log) tell_to(Err(msg), "slik")
        Left(msg)
      }
    }

    def is_finished (hash: String, name: String, log: Boolean): Boolean = {
      this.get_outcome(hash, name, log) match {
        case Right(out) => true
        case _ => false
      }
    }

    def wait (hash: String, names: List[String]): List[String] = {
      if (names.length == 0) return List(Statuses.SUCCESS)
      while (names.map(this.is_finished(hash, _, false)).exists(_ != true)) {
        Thread.sleep(100L)
      }
      names.map(this.get_outcome(hash, _, false) match {
        case Right(out) => out.outcome
        case _ => Statuses.FAILED
      })
    }

    def finalize(hash: String, name: String, outcome: Try[String]): Unit = {
      if (OUTCOMES.contains(hash)) {
        outcome match {
          case Success(out) => {
            RUNNING(hash).get(name) match {
              case Some(run) => {
                val payload = TaskPayload(name, Statuses.SUCCESS, out)
                OUTCOMES(hash).put(name, payload)
                run.runner ! Stop()
                tell_to(Info(s"Task ${name} completed with outcome ${Statuses.SUCCESS}"), "slik")
              }
              case None => {
                tell_to(Err(s"Task ${name} not found in run ${hash}."), "slik")
              }
            }
          }
          case Failure(ex) => {
            RUNNING(hash).get(name) match {
              case Some(run) => {
                val payload = TaskPayload(name, Statuses.FAILED, ex.getMessage())
                OUTCOMES(hash).put(name, payload)
                run.runner ! Stop()
                tell_to(Info(s"Task ${name} completed with outcome ${Statuses.FAILED}"), "slik")
              }
              case None => {
                tell_to(Err(s"Task ${name} not found in run ${hash}."), "slik")
              }
            }
          }
        }
      } else {
        tell_to(Err(s"${hash} not in OUTCOMES"), "slik")
      }
    }

    def cancel(hash: String, name: String): Either[String, String] = {
      RUNNING.get(hash) match {
        case Some(j) => j.get(name) match {
          case Some(run) => {
            if (run.status == Statuses.RUNNING) {
              val msg = s"task ${name} cancelled by user"
              val payload = TaskPayload(name, Statuses.CANCELLED, msg)
              OUTCOMES(hash).put(name, payload)
              run.runner ! Stop()
              Right(msg)
            } else {
              val msg = s"Nothing to do  to cancel task ${name}@${hash}, task not running"
              tell_to(Err(msg), "slik")
              Right(msg)
            }
          }
          case None => {
            val msg = s"Impossible to cancel task ${name}@${hash}, task not found"
            tell_to(Err(msg), "slik")
            Left(msg)
          }
        }
        case None => {
          val msg = s"Impossible to cancel task ${name}@${hash}, job not found"
          tell_to(Err(msg), "slik")
          Left(msg)
        }
      }
    }
  }

  object KVStore {

    def push(taskarg: TaskArg): Boolean = {
      if (ARGS.contains(taskarg.key)) {
        tell_to(Err(s"task argument ${taskarg.key} already within ARGS"), "slik")
        false
      } else {
        ARGS.put(taskarg.key, taskarg)
        tell_to(Info(s"task argument appended at key ${taskarg.key}"), "slik")
        true
      }
    }

    def pop(key: String): Option[TaskArg] = {
      if (ARGS.contains(key)) {
        tell_to(Info(s"task argument appended at key ${key}"), "slik")
        ARGS.remove(key)
      } else {
        tell_to(Err(s"task argument ${key} not in ARGS"), "slik")
        None
      }
    }

    def get(key: String): Option[TaskArg] = {
      if (ARGS.contains(key)) {
        tell_to(Info(s"task argument ${key} retrieved"), "slik")
        Some(ARGS(key))
      } else {
        tell_to(Err(s"task argument ${key} not in ARGS"), "slik")
        None
      }
    }

    def list(): Either[String,List[String]] = {
      if (ARGS.isEmpty) {
        val msg ="ARGS is empty nothing to list"
        tell_to(Info(msg), "slik")
        Left(msg)
      } else {
        tell_to(Info("Listing keys of ARGS"), "slik")
        Right(ARGS.keys.toList)
      }
    }

    def list(path: String): Option[Map[String,TaskArg]] = {
      val f = ARGS.filter((x: (String, TaskArg)) =>
        x._1.startsWith(path)).toMap
      if (f.isEmpty) None else Some(f)
    }
  }
}
