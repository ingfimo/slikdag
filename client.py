import requests
import json

class _SlikPrefix(object):

    _prefix = ""

    def __init__(self, host):
        self.path = f"{host}/{self._prefix}"


class KVStore(_SlikPrefix):

    _prefix = "kvstore"

    def push(self, key, dtype, value):
        body = json.dumps({"key": key, "dtype": dtype, "value": value})
        return requests.post(f"{self.path}/push", data=body)

    def pop(self, key):
        return requests.get(f"{self.path}/pop/{key}")

    def get(self, key):
        return requests.get(f"{self.path}/get/{key}")

    def list(self):
        return requests.get(f"{self.path}/list")

    
class Jobs (_SlikPrefix):

    _prefix = "jobs"

    def register(self, jobdef):
        return requests.post(f"{self.path}/register", data=jobdef)

    def intialize(self, key):
        return requests.get(f"{self.path}/initialize/{key}")

    def push(self, jobdef):
        return requests.post(f"{self.path}/push", data=jobdef)

    def get_def(self, key):
        return requests.get(f"{self.path}/get-def/{key}")

    def get_status(self, key):
        return requests.get(f"{self.path}/get-status/{key}")

    def get_outcome(self, key):
        return requests.get(f"{self.path}/get-outcome/{key}")

    def pop(self, key):
        return requests.get(f"{self.path}/pop/{key}")

    def list_def(self):
        return requests.get(f"{self.path}/list-def")

    def list_running(self):
        return requests.get(f"{self.path}/list-running")

    def list_outcomes(self):
        return requests.get(f"{self.path}/list-outcomes")


class Schedules(_SlikPrefix):

    _prefix = "schedules"

    def get(self, key):
        return requests.get(f"{self.path}/get/{key}")

    def stop(self, key):
        return requests.get(f"{self.path}/stop/{key}")

    def pop(self, key):
        return requests.get(f"{self.path}/pop/{key}")

    def list(self):
        return requests.get(f"{self.path}/list")


class Tasks(_SlikPrefix):

    _prefix = "tasks"

    def get_status(self, job, task):
        return requests.get(f"{self.path}/get-status/{job}/{task}")

    def get_outcome(self, job, task):
        return requests.get(f"{self.path}/get-outcome/{job}/{task}")

    def list_outcomes(self, job):
        return requests.get(f"{self.path}/list-outcomes")
    

class SlikClient(object):
    
    def __init__(self, host):
        self.host = host
        self.kvstore = KVStore(host)
        self.jobs = Jobs(host)
        self.schedules = Schedules(host)
        self.tasks = Tasks(host)

        
